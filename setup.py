import commands
from distutils.core import setup

setup(name='mpDris',
		version='0.1',
		description='A MPD client implementing the XMMS2 MPRIS interface',
		author='Erik Karlsson',
		author_email='pilo@ayeon.org',
		url='http://ayeon.org/projects/mpDris/',
		scripts=[ 'mpDris' ],
		data_files=[ (commands.getoutput('pkg-config --variable=session_bus_services_dir dbus-1'), [ 'org.mpris.mpd.service' ]) ]
	 )

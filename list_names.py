#!/usr/bin/python

import dbus

session_bus = dbus.SessionBus()

dbus = session_bus.get_object("org.freedesktop.DBus", "/org/freedesktop/DBus")
names = dbus.ListNames(dbus_interface = "org.freedesktop.DBus")
for name in names:
	print name
